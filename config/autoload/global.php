<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'download_options' => [
        'timeout'            => 15,
        'connection_timeout' => 15,
        'useragent'          => "Mozilla/5.0 (compatible; CDN-WebexDigital/2.0; +https://webex.digital/cdn)",
    ]
];
