<?php

namespace CDN\Service;

use GuzzleHttp;
use \DirectoryIterator;

class StorageManager
{
    private $config       = [];
    private $domains      = [];
    private $filename     = '';
    private $origin       = '';
    private $storage_path = '';
    private $expiration   = 86400;

    public function __construct(array $config)
    {
        $this->config = $config;

        if (!empty($this->config['domains'])) {
            $this->domains = $this->config['domains'];
        }
    }

    private function fileExists()
    {
        if (!file_exists($this->storage_path . $this->encodedFilename($this->filename))) {
            return false;
        }

        if (\filemtime($this->storage_path . $this->encodedFilename($this->filename)) < time() - $this->expiration) {
            return false;
        }

        return true;
    }

    private function getFilename()
    {
        if (!file_exists($this->storage_path)) {
            mkdir($this->storage_path, 0777, true);
        }

        if (!is_writable($this->storage_path)) {
            throw new \RuntimeException("Storage path '" . $this->storage_path . "' is not writable!");
        }

        return $this->storage_path . $this->encodedFilename($this->filename);
    }

    private function getMetaFilename()
    {
        if (!file_exists($this->storage_path)) {
            mkdir($this->storage_path, 0777, true);
        }

        if (!is_writable($this->storage_path)) {
            throw new \RuntimeException("Storage path '" . $this->storage_path . "' is not writable!");
        }

        return $this->storage_path . $this->encodedFilename($this->filename) . '.meta.json';
    }

    private function truncate(string $filename)
    {
        return ltrim(trim($filename), '/');
    }

    private function encodedFilename(string $filename)
    {
        return str_replace('/', '_', $this->truncate($filename));

        // return strtr(base64_encode(
        //     $this->truncate($filename)
        // ), '+/=', '-_,');
    }

    public function get(string $host, string $filename)
    {
        if (empty($this->domains[$host])) {
            return false;
        }

        // Truncate filename
        $this->filename = $this->truncate($filename);
        // Storage path for specific domain
        $this->storage_path = $this->domains[$host]['storage_path'];
        // Origin url
        $this->origin = rtrim($this->domains[$host]['origin'], '/') . '/';
        // Cache file expiration
        $this->expiration = (int) isset($this->domains[$host]['expiration']) ? $this->domains[$host]['expiration'] : 86400;

        // If file not exists, download and save this file
        if (!$this->fileExists()) {
            $this->download();
        }

        // Retrieve file
        return $this->retrieve();
    }

    private function retrieve()
    {
        if (!$this->fileExists()) {
            return false;
        }

        $file = $this->getFilename();

        $ret                = new \stdClass();
        $ret->expiration    = $this->expiration;
        $ret->filename      = $this->filename;
        $ret->origin        = $this->origin;
        $ret->mime_type     = \mime_content_type($file);
        $ret->last_modified = \filemtime($file);
        $ret->content       = \file_get_contents($file);
        $ret->headers       = null;

        if (file_exists($this->getMetaFilename())) {
            $meta = \json_decode(\file_get_contents($this->getMetaFilename()), true);

            if (!empty($meta['headers'])) {
                $ret->headers = array_map(function($value) {
                    return reset($value);
                }, $meta['headers']);
            }
        }

        return $ret;
    }

    private function download()
    {
        $client = new GuzzleHttp\Client([
            'base_uri' => $this->origin,
            'headers' => [
                'user-agent' => $this->config['download_options']['useragent']
            ]
        ]);

        $request = new GuzzleHttp\Psr7\Request('GET', $this->filename);

        $promise = $client->sendAsync($request)->then(function ($response) {
            // Header response code is 200
            if ($response->getStatusCode() == 200) {
                $this->persistHeaders($response->getHeaders());
                $this->persist((string) $response->getBody());
            }
        });

        $promise->wait();
    }

    private function persistHeaders(array $headers)
    {
        // Write to content to file
        file_put_contents($this->getMetaFilename(), json_encode(['headers' => $headers]), LOCK_EX);
    }

    private function persist(string $content)
    {
        // $content = \gzcompress($content, 9);

        // Write to content to file
        file_put_contents($this->getFilename(), $content, LOCK_EX);
    }

    public function flush(string $domain = '')
    {
        $domains = [];

        // If $domain is set, then flush only this content
        if (!empty($domain)) {
            if (!empty($this->domains[$domain])) {
                $domains = [$this->domains[$domain]];
            }
        }

        // Flush all domains content
        if (empty($domains)) {
            foreach ($this->domains as $domain) {
                $domains[] = $domain;
            }
        }

        if (!empty($domains)) {
            // storage_path, expiration
            foreach ($domains as $domain) {
                // Check if directory exists
                if (is_dir($domain['storage_path'])) {
                    $dir = new DirectoryIterator($domain['storage_path']);
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {

                            // If cache file is older, then remove it
                            if ($fileinfo->getMTime() < time() - (int) $domain['expiration']) {

                                if (!is_writable($fileinfo->getPathname())) {
                                    throw new \RuntimeException("File '" . $fileinfo->getPathname() . "' is not writable!");

                                } else {
                                    unlink($fileinfo->getPathname());
                                }
                            }
                        }
                    }
                }
            }

        } else {
            return false;
        }

        return true;
    }
}
