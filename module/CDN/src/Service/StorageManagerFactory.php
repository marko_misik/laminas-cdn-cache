<?php

namespace CDN\Service;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class StorageManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Instantiate the service and inject dependencies
        return new $requestedName(
            $container->get('Config')
        );
    }
}
