<?php

declare(strict_types=1);

namespace CDN\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Console\Request as ConsoleRequest;
use Laminas\Validator\File\MimeType;
use CDN\Service\StorageManager;

class ServeController extends AbstractActionController
{
    protected $uri;
    protected $path;

    /**
     * @var \CDN\Service\StorageManager
     */
    protected $storageManager;

    public function __construct(StorageManager $storageManager)
    {
        $this->storageManager = $storageManager;
    }

    public function retrieveAction()
    {
        $this->uri = $this->getRequest()->getUri();
        $this->filename = str_replace('\\', '/', urldecode((string) $this->params()->fromRoute('path')));

        if (empty($this->filename)) {
            // Set 404 Not Found status code
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Retrieve file
        $file = $this->storageManager->get($this->uri->getHost(), $this->filename);

        // Write HTTP headers
        $response = $this->getResponse();
        $headers = $response->getHeaders();

        if ($file->headers) {
            $file->mime_type = $file->headers['Content-Type'];
        }

        $headers->addHeaderLine("Accept-Ranges: bytes");
        $headers->addHeaderLine("Access-Control-Allow-Credentials: true");
        $headers->addHeaderLine("Access-Control-Allow-Origin: *");
        $headers->addHeaderLine("Pragma: public");
        $headers->addHeaderLine("Cache-Control: max-age=" . $file->expiration);
        $headers->addHeaderLine("Content-type: " . $file->mime_type);
        $headers->addHeaderLine("Last-Modified: " . gmdate('D, d M Y H:i:s \G\M\T', $file->last_modified));
        $headers->addHeaderLine("Expires: " . gmdate('D, d M Y H:i:s \G\M\T', time() + $file->expiration));

        // Write file content
        if($content = $file->content) {
            $response->setContent($content);

        } else {
            // Set 404 Not Found status code
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Return Response to avoid default view rendering
        return $this->getResponse();
    }

    public function flushAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console, and the user has not
        // tricked our application into running this action from a public web
        // server:
        if (! $request instanceof ConsoleRequest) {
            throw new RuntimeException('You can only use this action from a console!');
        }

        $domain = $request->getParam('domain', '');

        if ($status = $this->storageManager->flush($domain)) {

            if (!empty($domain)) {
                return "Done! CDN cache for domain '$domain' has been flushed.\n";

            } else {
                return "Done! CDN cache has been flushed.\n";
            }

        } else {

            if (!empty($domain)) {
                return "Error! CDN cache for domain '$domain' not found! Try to check domains.config file to ensure, that domain name is set correctly.\n";

            } else {
                return "Error! Flushing CDN cache exited with ERROR. There's not domains configured! Try to check domains.config file.\n";
            }
        }
    }
}
