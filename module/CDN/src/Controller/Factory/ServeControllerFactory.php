<?php

namespace CDN\Controller\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use CDN\Service\StorageManager;

class ServeControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Instantiate the service and inject dependencies
        return new $requestedName(
            $container->get(StorageManager::class)
        );
    }
}
