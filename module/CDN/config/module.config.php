<?php

declare(strict_types=1);

namespace CDN;

use Laminas\Router\Http\Hostname;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\Router\Http\Regex;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'console' => [
        'router' => [
            'routes' => [
                'flush_cdn_caches' => [
                    'options' => [
                        'route'    => 'cdn-cache flush [<domain>]',
                        'defaults' => [
                            'controller' => Controller\ServeController::class,
                            'action'     => 'flush',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'cdn_route' => [
                        'type'    => Regex::class,
                        'options' => [
                            'regex' => '(?<path>.*)?',
                            'defaults' => [
                                'controller' => Controller\ServeController::class,
                                'action'     => 'retrieve',
                                'path'       => '',
                            ],
                            'spec' => '%path%',
                        ],
                        'may_terminate' => true,
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\ServeController::class => Controller\Factory\ServeControllerFactory::class,
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\StorageManager::class => Service\StorageManagerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'   => __DIR__ . '/../view/layout/layout.phtml',
            'cnd/index/index' => __DIR__ . '/../view/cnd/index/index.phtml',
            'error/404'       => __DIR__ . '/../view/error/404.phtml',
            'error/index'     => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
